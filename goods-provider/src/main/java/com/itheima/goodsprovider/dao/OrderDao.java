package com.itheima.goodsprovider.dao;

import com.itheima.b2b.commonmodule.model.Userorder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Classname OrderDao
 * @Description TODO
 * @Date 2019-10-23 9:31
 * @Created by CrazyStone
 */
@Component
@Mapper
public interface OrderDao {
    List<Userorder> getAllorder(@Param(value = "uid")int uid);//查询所有订单
}
