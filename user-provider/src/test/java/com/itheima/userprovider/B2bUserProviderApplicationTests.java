package com.itheima.userprovider;

import com.itheima.b2b.commonmodule.model.User;
import com.itheima.userprovider.dao.UserDao;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class B2bUserProviderApplicationTests {
    @Autowired
    private UserDao userDao;

    @Test
    public void contextLoads() {
    }

    @Test
    public void testRegister(){
      //  System.out.println(userDao.register("123","tom","男"));
        User user = userDao.login("王三");
        Assert.assertEquals( "王三",user.getUname());
        System.out.println(user);
    }
}
