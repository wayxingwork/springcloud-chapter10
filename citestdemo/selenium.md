## Selenium 自动化测试入门

#### 基于webdriver

1. 查看浏览器版本号

2. https://npmmirror.com/package/chromedriver 下载具体版本

3. 解压放入固定位置

4. 添加依赖
```
    <dependency>
        <groupId>org.seleniumhq.selenium</groupId>
        <artifactId>selenium-java</artifactId>
        <version>3.14.0</version>
    </dependency>
```

#### 参考资料

- https://www.bilibili.com/video/BV1TZ4y1j7Nk?p=1 B站真是好地方

- https://blog.csdn.net/ymxkybqw/article/details/85271026

