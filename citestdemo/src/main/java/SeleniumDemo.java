import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumDemo {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "D:\\Backup\\Documents\\Downloads\\chromedriver_win32-104\\chromedriver.exe");
        // 实例化 Chrome/Chromium 会话
        WebDriver driver = new ChromeDriver();
        driver.get("http://localhost:5000/api-a/admin/login");
        System.out.println("The testing page title is: " + driver.getTitle());
        //自动向网页写入名字与密码
        driver.findElement(By.id("userName")).sendKeys("王三");
        driver.findElement(By.id("password")).sendKeys("123");
        driver.findElement(By.id("btn-ty")).click();
//		driver.quit();	//退出
    }
}
