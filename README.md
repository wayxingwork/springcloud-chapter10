# springcloud-idea-chapter10-goods

#### 介绍
Spring cloud 实战电商网站项目( 模块)

目的： 综合应用 spring cloud进行微服务架构开发。

#### 开发环境

操作系统 ： windows

Java环境 ： JDK1.8(不能使用高版本)

开发工具 ： Idea 2021

数据库： mysql 5.5以上
 
spring cloud : Greenwich.SR2

spring boot : 2.1.7 Release

#### 测试方法

- 数据库使用本机localhost配置MySQL b2bdata.sql 和 b2bgoods.sql。
- common 模块中 install
- 启动 eureka-server
- 启动 user-provider,注意修改数据库连接密码
- 启动 user-consumer
- 访问 http://localhost:8893/admin/tologin

  王三 ， 123

- goods-provider 修改 application.yml，注意数据库配置
 
- 运行 goods-provider 中的单元测试程序

访问 http://localhost:8896/goods/getAll

![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/044403_c032b758_382074.png "屏幕截图.png")